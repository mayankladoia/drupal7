initial commit contains:

1. Add the following modules to your Drupal install:
a. Features - downloaded, installed, enabled
b. Ctools - downloaded, installed, enabled
c. Pathauto - downloaded, installed, enabled
d. Coffee - downloaded, installed, enabled
e. Facet API - downloaded, installed, enabled
f. Panels - downloaded, installed, enabled
g. Views - downloaded, installed, enabled
h. Views UI - downloaded, installed, enabled
2. Create 3 basic pages that use Panels in place editor. Name one page welcome 
	created 3 pages - /welcome, /page2, /page3
3. Create any custom content type with any fields
	created page /mayank

4. Generate some content (nodes, and your custom content type)
a. Hint: there are modules that do this
5. Utilize views to dynamically generate a list of nodes that display in ascending publish date
	created view /testview